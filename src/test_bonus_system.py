from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"


def test_invalid_cases():
    assert calculateBonuses("", 1000) == 0
    assert calculateBonuses("", 10000) == 0
    assert calculateBonuses("", 25000) == 0
    assert calculateBonuses("", 50000) == 0
    assert calculateBonuses("", 75000) == 0
    assert calculateBonuses("", 100000) == 0
    assert calculateBonuses("", 250000) == 0
    assert calculateBonuses("A", 1000) == 0
    assert calculateBonuses("Z", 1000) == 0
    assert calculateBonuses("Dia", 1000) == 0


def test_standard_program():
    assert calculateBonuses(STANDARD, 1000) == 0.5
    assert calculateBonuses(STANDARD, 10000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 25000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 50000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 75000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 100000) == 0.5 * 2.5
    assert calculateBonuses(STANDARD, 250000) == 0.5 * 2.5


def test_premium_program():
    assert calculateBonuses(PREMIUM, 1000) == 0.1
    assert calculateBonuses(PREMIUM, 10000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 25000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 50000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 75000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 100000) == 0.1 * 2.5
    assert calculateBonuses(PREMIUM, 250000) == 0.1 * 2.5


def test_diamond_program():
    assert calculateBonuses(DIAMOND, 5000) == 0.2
    assert calculateBonuses(DIAMOND, 10000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 25000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 50000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 70000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 100000) == 0.2 * 2.5
    assert calculateBonuses(DIAMOND, 250000) == 0.2 * 2.5